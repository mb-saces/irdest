---
Contribute_Title: Want to learn more?
Contribute: |
    **Do you have questions about the project or want to talk to us about
    irdest?  Check out the [Community](/community) page to learn how.**

    The project is currently undergoing a rewrite.  Many of the core tools
    are already done, but user-facing applications are lacking.  The easiest
    way to get into **irdest** is to download the Linux `irdest-hubd` binary
    and joining the open irdest test network.
    [Find out how!](/downloads#irdest-hubd)
    
    If you have trouble setting up the hubd server, check out the 
    [Learn](/learn#manuals) page to find user manuals, and documentation
    on both code-internals, and irdest protocol descriptions.
Funding_Title: Funding & Partnerships
Funding: |
    The Irdest project does not work alone, and we are always looking for
    collaboration opportunities.  Please don't hesitante to contact us!

    <div class="sponsors">
        <a href="https://nlnet.nl/project/Irdest/"><img class="sponsor" src="img/nlnet.svg" /></a>
        <a href="https://nlnet.nl/NGI0/"><img class="sponsor" src="img/NGIZero-green.hex.svg" /></a>
        <a href="https://summerofcode.withgoogle.com/projects/#4792427082153984"><img class="sponsor" src="img/GSoC.svg" /></a>
        <a href="https://freifunk.net"><img class="sponsor" src="img/freifunk.svg" /></a>
    </div>
---
