# Available clients

While a lot of the core tools for irdest already exist, the user-facing
applications are mostly still prototypes.  Following is a list of
supported clients/ platforms with instructions on how to build, and
configure them.


| Client name   | Desktop platforms | Mobile platform | Version |
|---------------|-------------------|-----------------|---------|
| [irdest-hubd] | Linux, MacOS      | -               | 0.1.0   |
| [irdest-gtk]  | Linux             | -               | 0.1.0   |
| [irdestdroid] | -                 | Android         | 0.1.0   |

[irdest-hubd]: ./irdest-hubd/index.md
[irdest-gtk]: ./irdest-gtk/index.md
[irdestdroid]: ./irdestdroid/index.md
