# irdest-hubd

This is a multi-purpose headless client, meaning that it runs as a
daemon in the background of your system.  It bundles the irdest
router, database, core services, and RPC broker.

Hubd doesn't come with its own user interface, which means you need to
install one yourself.  Currently recommended is [irdest-gtk].

Hubd does not require administrative privileges.

[irdest-gtk]: ../irdest-gtk
