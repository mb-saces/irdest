# Summary

- [Available clients](./clients.md)
- [Nix builds](./nix.md)
- [Building this manual](./manual.md)
- [irdest-hubd](./irdest-hubd/index.md)
  - [Manual build](./irdest-hubd/build.md)
  - [Configuration](./irdest-hubd/config.md)
- [irdest-gtk](./irdest-gtk/index.md)
- [irdest-droid](./irdest-droid/index.md)
