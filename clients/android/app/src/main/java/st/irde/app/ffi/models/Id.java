package st.irde.app.ffi.models;

/**
 * Represents a user ID in irdest
 */
public class Id {
    public String inner;

    public Id(String inner) {
        this.inner = inner;
    }
}
