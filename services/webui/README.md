# Service: webui

This service provides an HTTP API and a corresponding cross-platform
webui (written in ember.js) for the core Irdest services.

To use this UI, consult the list of [clients]() that ship this
front-end.  To work on the front end, see the
[`./develop.sh`](./develop.sh) script to live-load changes you make to
this directory.
