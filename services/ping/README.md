# ping service

A minimal qaul service, pinging other instances of itself on the
network.  This service can be used by your application to collect
organic connection quality metrics to other participants on the
network.


